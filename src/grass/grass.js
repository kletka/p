import { times } from "ramda";
import { gradient, gcd } from "help";
import { WIDTH, HEIGHT } from "consts";

const TILE = gcd(WIDTH, HEIGHT) / 10
const THRESHOLD = Math.floor(HEIGHT / TILE / 2);

export const filler = function (threshold, width, height, tile) {
  for (let col = 0; col <= width/tile; col++) {
    this.fill(50, 150, 50)
    let startPoint = threshold + Math.floor(this.randomGaussian(0, 1))
    this.rect(tile * col, height - (startPoint-- * tile), tile, tile);

    times((n) => {
      this.fill(this.random(105), 70, 50)
      this.rect(tile * col, height - (startPoint-- * tile), tile, tile);
    }, 3);

    for (let line = startPoint; line > 0; line--) {
      this.fill((100 + this.randomGaussian(0, 5)), 50, 50)
      this.rect(tile * col, height - (line * tile), tile, tile);
    }
  }
}

export const setup = function () {
  this.createCanvas(WIDTH, HEIGHT);
  this.frameRate(5);
}

export const draw = function () {
  gradient.call(this, 0, 0, WIDTH, HEIGHT, this.color(150, 100, 200), this.color(0, 100, 100));
  this.noStroke();
  filler.call(this, THRESHOLD, WIDTH, HEIGHT, TILE)
};
