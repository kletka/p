export default function (x, y, w, h, c1, c2) {
  this.noFill();
  for (let i = y; i <= y + h; i++) {
    this.stroke(this.lerpColor(c1, c2, this.map(i, y, y+h, 0, 1)));
    this.line(x, i, x + w, i);
  }
}
