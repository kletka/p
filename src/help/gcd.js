export default function calc(a, b) {
  return b ? calc(b, a % b) : a;
}
