import _gradient from "./gradient";
import _gcd from "./gcd";
import _axes from "./axes";

export const gradient = _gradient;
export const gcd = _gcd;
export const axes = _axes;
