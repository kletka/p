export default function () {
  /* Y AXIS */
  this.push();
  this.noStroke();
  this.translate(0, 50000, 0);
  this.fill(0, 255, 0);
  this.box(2, 100000, 2);
  this.pop();

  /* Z AXIS */
  this.push();
  this.noStroke();
  this.translate(0, 0, 50000);
  this.rotateX(this.HALF_PI);
  this.fill(0, 0, 255);
  this.box(2, 100000, 2);
  this.pop();

  /* X AXIS */
  this.push();
  this.noStroke();
  this.translate(50000, 0, 0);
  this.fill(255, 0, 0);
  this.rotateX(this.HALF_PI);
  this.rotateZ(this.HALF_PI);
  this.box(2, 100000, 2);
  this.pop();
}
