import p5 from "p5";
import {
  setup,
  draw,
  preload,
  keyPressed,
  mouseMoved
} from "./iso";

const sketch = (p) => {

  p.mouseMoved = mouseMoved && mouseMoved.bind(p);
  p.keyPressed = keyPressed && keyPressed.bind(p);
  p.preload = preload && preload.bind(p);
  p.setup = setup.bind(p);
  p.draw = draw.bind(p);
}

new p5(sketch, "p5-wrap");
