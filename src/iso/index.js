import { WIDTH, HEIGHT, VELOCITY } from "consts";
import { axes } from "help";

const SIZE = 100;

const TERRAIN_SIZE = 10 * SIZE;

const CAMERA_THRESHOLD = 20;

export const setup = function () {
  this.createCanvas(WIDTH, HEIGHT, this.WEBGL);
  this.frameRate(25);
  this.ortho(-200, 200, -200, 100, 10000000, 0);
  this.viewport = new Camera(this)
}

export const draw = function () {
  this.background(100, 50, 100);
  axes.call(this);
  this.viewport.render()

  this.push();
  this.translate(TERRAIN_SIZE / 2, TERRAIN_SIZE / 2, 0)
  this.fill(100, 30, 60);
  this.plane(TERRAIN_SIZE, TERRAIN_SIZE);
  this.pop();

};


class Camera {
  constructor (context) {
    this.p = context;
    this.pos = { x: -200, y: 200, z: -200 };
    this.view = { x: 0, y: 0, z: 0 };
  }

  mouse () {
    const { p, height, width } = this;

    const dx = p.mouseY;
    const dy = p.mouseX;

    if (HEIGHT - dx < CAMERA_THRESHOLD ) {
      this.pos.x += VELOCITY;
      this.view.x += VELOCITY;
    }

    if (dx < CAMERA_THRESHOLD) {
      this.pos.x -= VELOCITY;
      this.view.x -= VELOCITY;
    }

    if (WIDTH - dy < CAMERA_THRESHOLD) {
      this.pos.y += VELOCITY;
      this.view.y += VELOCITY;
    }

    if (dy < CAMERA_THRESHOLD) {
      this.pos.y -= VELOCITY;
      this.view.y -= VELOCITY;
    }

  }

  render() {
    const { p, pos, view } = this;
    this.mouse()
    p.camera(pos.x, pos.y, pos.z, view.x, view.y, view.z, 0, 0, -1);
  }
}
