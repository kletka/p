import { times } from "ramda";
import { WIDTH, HEIGHT } from "consts";

const POINTS = 20;
const LIMIT = 1;
const LOOP = !false;
const FRAMERATE = 15;

const [CENTERX, CENTERY] = [WIDTH/2, HEIGHT/2];

const STEPX = WIDTH / POINTS;
const STEPY = HEIGHT / POINTS;

const setState = function (data) {
  this.state = Object.assign({}, this.state, data);
}

const updateState = function (state, cb) {
  if (this.state.loop && this.state.repetitions === this.state.limit) {
    return this.setState({
      current: [1, 1],
      repetitions: 0,
      fn: cb.bind(this)
    });
  }

  return this.setState(state);
}

const drawPoint = function (x, y) {
  this.stroke(255)
  this.point((x * STEPX), (y * STEPY));
  this.stroke(255, 50)
  this.line((x * STEPX), (y * STEPY), CENTERX, CENTERY);

}

function nextPoint () {
  let [x, y] = this.state.current;
  let { repetitions } = this.state;

  if (x === POINTS - 1) {
    [x, y] = [1, ++y];
  } else if (y === POINTS) {
    [x, y] = [++x, 1];
  } else if (x * y !== (POINTS - 2) * (POINTS - 2)) {
    [x, y] = [++x, y];
  } else {
    [x, y] = [1, 1];
    repetitions += 1;
  }
  drawPoint.call(this, x, y);
  updateState.call(this, {current: [x, y], repetitions}, nextLineV.bind(this))
}

function nextLineV () {
  let [x, y] = this.state.current;
  let { repetitions } = this.state;

  if (y === POINTS - 1) {
    [x, y] = [++x, 1];
    repetitions += 1;
  } else {
    [x, y] = [1, ++y];
  }

  for (x = 1; x < POINTS; x++) drawPoint.call(this, x, y);


  updateState.call(this, {current: [x, y], repetitions}, nextLineH.bind(this))
}

function nextLineH () {
  let [x, y] = this.state.current;
  let { repetitions } = this.state;

  if (x === POINTS - 1) {
    [x, y] = [1, ++y];
    repetitions += 1;
  } else {
    [x, y] = [++x, 1];
  }

  for (y = 1; y < POINTS; y++) drawPoint.call(this, x, y);
  updateState.call(this, {current: [x, y], repetitions}, nextRandom.bind(this))
}

function nextRandom () {
  let [x, y] = this.state.current;
  let { repetitions } = this.state;
  if (x === POINTS - 1) {
    [x, y] = [1, ++y];
  } else if (y === POINTS) {
    [x, y] = [++x, 1];
  } else if (x * y !== (POINTS - 2) * (POINTS - 2)) {
    [x, y] = [++x, y];
  } else {
    [x, y] = [1, 1];
    repetitions += 1;
  }
  for (let i = 0; i < x; i++) drawPoint.call(this, this.random(POINTS), this.random(POINTS));
  updateState.call(this, {current: [x, y], repetitions}, nextPoint.bind(this))
}


export const setup = function () {
  this.createCanvas(WIDTH, HEIGHT);
  this.background(0);
  this.frameRate(FRAMERATE);
  this.setState = setState.bind(this);
  this.state = {
    limit: LIMIT,
    loop: LOOP,
    repetitions: 0,
    current: [1, 1],
    fn: nextRandom.bind(this),
  }
}

export const draw = function () {
  this.background(0);
  this.state.fn();
}
