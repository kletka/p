export const WIDTH = 640;
export const HEIGHT = 480;

export const STEP3D = 10;
export const VELOCITY = 25;


export const KEY_W = 87;
export const KEY_D = 68;
export const KEY_S = 83;
export const KEY_A = 65;
